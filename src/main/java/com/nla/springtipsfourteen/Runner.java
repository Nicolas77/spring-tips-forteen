package com.nla.springtipsfourteen;

import com.nla.springtipsfourteen.domain.EmotionalState;
import com.nla.springtipsfourteen.domain.PeopleService;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class Runner {

    private final PeopleService peopleService;
    Runner(PeopleService peopleService) {
        this.peopleService = peopleService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void exercise() {
        var elizabeth = this.peopleService.create("Elizabeth", EmotionalState.SAD);
        System.out.println(elizabeth);
    }
}
