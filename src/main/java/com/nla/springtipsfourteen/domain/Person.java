package com.nla.springtipsfourteen.domain;

public record Person(Integer id, String name, int emotionalState) {
}
