package com.nla.springtipsfourteen.domain;

public enum EmotionalState {
    SAD, HAPPY, NEUTRAL
}
