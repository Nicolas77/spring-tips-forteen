package com.nla.springtipsfourteen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringTipsForteenApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringTipsForteenApplication.class, args);
	}

}
